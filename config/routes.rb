Rails.application.routes.draw do
  api_version(:module => "V1", :path => {:value => "v1"}) do
  	resources :barters
  	resources :products
  	resources :product_barters
  	resources :users
  	resources :villages
    post 'users/login' => 'users#login'
    post 'users/auth_token' => 'users#auth_token'
  end
	constraints subdomain: 'api' do
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
